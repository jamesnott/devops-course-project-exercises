FROM python:3.10-slim-buster AS base

# switch working directory
WORKDIR /app

# install curl and poetry
RUN apt-get update -y
RUN apt-get install -y curl
RUN echo "base"
RUN curl -sSL https://install.python-poetry.org | python3 -
ENV PATH=$PATH:/root/.local/bin

# copy all content from the local file to the image and install poetry 
COPY . /app
RUN poetry install

# build a production image based on the base image
FROM base AS production
RUN echo "production"
CMD poetry run gunicorn --bind 0.0.0.0 "todo_app.app:create_app()"

# build a development image based on the base image
FROM base AS development
EXPOSE 8000
RUN echo "development"
CMD poetry run flask run --host=0.0.0.0

# testing stage
FROM base as test
ENTRYPOINT ["poetry", "run", "pytest"]