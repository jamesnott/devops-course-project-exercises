import { test, expect } from '@playwright/test';

const TODO_ITEMS = [
    'Attend Workshop',
    'Arrange Support Call',
    'Complete Exercise',
    'Update APTEM'
  ];

test.beforeEach(async ({ page }) => {
    await page.goto('https://jn-todo-app.azurewebsites.net/');
  });

test('Does the JN Todo home page appear correctly?', async ({ page }) => {
  await expect(page.getByRole('heading', { name: 'To-Do App' })).toBeVisible();
  await expect(page.getByRole('heading', { name: 'New Task' })).toBeVisible();
  await expect(page.getByRole('heading', { name: 'To Do' })).toBeVisible();

});

test('should clear text input field when an item is added', async ({ page }) => {
  // create a new todo locator
  const newTodo = page.getByRole('textbox');

  // Create one todo item.
  await newTodo.fill(TODO_ITEMS[0]);
  await page.getByRole('button', { name: 'Submit' }).click()

  // Check that input is empty.
  page.waitForSelector.name;
  await expect(newTodo).toBeEmpty();
});

//test('Check new item is added', async ({ page }) => {
  // Find the textbox
  //const textbox = await page.locator('input');

  // Evaluate the text content of the textbox
  //const textContent = await textbox.evaluate((element) => element.textContent);

  // Assert that the textbox contains the specific text
  //expect(textContent).toContain(TODO_ITEMS[0]);
  
  //await expect(textbox).toContain(TODO_ITEMS[0]);


//});
