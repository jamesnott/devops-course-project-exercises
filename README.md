# DevOps Apprenticeship: Project Exercise

> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.7+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py -UseBasicParsing).Content | python -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

## Integration with Trello
Have updated the ToDo list application to integrate with Trello in the following way: 
1) Whenever a new item is added on the home page, it will be added to the 'To Do' list. This is also reflected in Trello.
2) When a an item in the To Do, Doing or Done list is moved via the buttons on the web page, the page will re-render and it will appear in its new list. This will also be reflected in Trello.

## TDD, Pytest & Unit Testing
* Unit testing focuses on checking whether or not the software is behaving correctly, which means checking that the mapping between the inputs and the outputs are all done correctly. 
* Being a low-level testing activity, unit testing helps in the early identification of bugs so that they aren’t propagated to higher levels of the software system.
* Unit Tests use the Pytest add in and are documented in this file: test_view_model.py
* Run the Unit Tests in Test Explorer from the menu on the left of VSCode.
* Unit Tests were written as part of the TDD methodology, i.e. before the code was written itself.
* Please add more tests to the file as you add more features.

## Containerisation with Docker - Key Commands
Open the Docker desktop application as this will start to run the Docker service. 

### To build the development container:
```bash
docker build --target development --tag todo-app:dev .
```
### To run the development container:
Note - you need to change your port mapping by default, i.e. the -p <host>:<container> part of your docker run command, e.g. specifying -p 8080:5000 should match localhost:8080 on your machine to the 5000 port inside the container.
```bash
docker run -it --env-file ./.env -p 8080:5000 todo-app:dev 
```
### To run the development container with a binded mount:
```bash
docker run -it --env-file ./.env -p 8080:5000 --mount type=bind,source="$(pwd)"/todo_app,target=/app/todo_app todo-app:dev
```
### To build the production container:
```bash
docker build --target development --tag todo-app:prod .
```
### To run the production container:
```bash
docker run -it --env-file ./.env -p 8080:8000 todo-app:prod
```
## Automated tests via Docker
*It is now possible to run the unit, integration pytests within the test stage in the Docker image. End-to-end tests currently out of scope. Those can be run with:
```
docker build --target test --tag my-test-image .
docker run my-test-image
```

## Put Container Image on Docker Hub registry
Log into DockerHub locally:
```bash
docker login
```
Build the image:
```bash
docker build --target development --tag jn-todo-app:latest .  
```
Push the image:
```bash
docker push notty6/jn-todo-app:latest 
```

## Create Web App on Azure
```
https://jn-todo-app.azurewebsites.net/
```

## MongoDB
```bash
az cosmosdb keys list -n "jamnotcosmos" -g "Cohort23-24_JamNot_ProjectExercise" --type connection-strings
poetry run python 

>>> import pymongo
>>> client = pymongo.MongoClient("mongodb://jamnotcosmos:olXyZMKoAxhA8tnE2A6B9z6xYqtxRkbflJebZ3zv17kWby4PG0GHJmrmV1rBhfsAWV3L7tdrSETEACDbAcuGng==@jamnotcosmos.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@jamnotcosmos@") 
>>> client.list_database_names()
['jamnotdb']
```

## Authentication
Have updated the ToDo list application to handle authentication via OAuth 2. OAuth 2.0 provides secure delegated server resource access to client applications on behalf of a resource owner. OAuth 2 allows authorization servers to issue access tokens to third-party clients with the approval of the resource owner or the end-user.

After adding an OAuth 2 application to an instance, you can use OAuth 2 to:

Enable users to sign in to your application with their GitLab.com account.
Set up GitLab.com for authentication to your GitLab instance. For more information, see integrating your server with GitLab.com.

After an application is created, external services can manage access tokens using the OAuth 2 API.
Create a user-owned application