import requests, os, jwt
from flask import Flask, render_template, request, redirect
from flask_login import LoginManager, login_required, login_user, current_user
from todo_app.flask_config import Config
from todo_app.data.mongo_items import get_items, add_item, update_items_todo, update_items_doing, update_items_done
from todo_app.data.view_model import ViewModel
from todo_app.data.item import Item
from todo_app.my_user import User



def create_app():   
    app = Flask(__name__, static_url_path='/static')
    app.config.from_object(Config())
    app.config['LOGIN_DISABLED'] = os.getenv('LOGIN_DISABLED') == 'True'
    login_manager = LoginManager()
    login_manager.init_app(app)
        
    @login_manager.unauthorized_handler
    def unauthenticated():
        REDIRECT_URL = f"https://gitlab.com/oauth/authorize?client_id={os.getenv('ApplicationID')}&redirect_uri={request.host_url}login/callback&response_type=code&scope=openid"
        return redirect(REDIRECT_URL)

    @login_manager.user_loader
    def load_user(user_id):
        id=user_id
        return User(id)
        
    @app.route('/login/callback')
    def callback():
        #After logging in, user is redirected to a “callback” URL.
        code = request.args.get('code')
        REDIRECT_URI = f"{request.host_url}login/callback"
        print(code)
        print(REDIRECT_URI)
        
        #Exchange the code for an “access token” and “id token” by sending a request to GitLab
        parameters = {"client_id"     : os.getenv("ApplicationID"),
                      "client_secret" : os.getenv("client_secret"),
                      "grant_type"    : "authorization_code",
                      "code"          : code,
                      "redirect_uri"  : REDIRECT_URI}
        
        #retrieves the user’s data from GitLab
        response = requests.post('https://gitlab.com/oauth/token/', data = parameters)
        response.raise_for_status()
        
        #identify the user’s id_token in the response
        response_json = response.json()
        id_token = response_json['id_token']
        print(id_token)
        
        #decode the json to get the "sub"
        decode_result = jwt.decode(id_token, options={"verify_signature": False})
        print(decode_result)
        subject = decode_result['sub']
        print(subject)
        myuser = User(id=subject)
        print(myuser)
        
        #login user with correct id
        login_user(myuser, remember=True, duration=None, force=False, fresh=True)
        return redirect('/')
        
    @app.route('/')
    @login_required
    def index():
        items: list[Item] = get_items()
        item_view_model = ViewModel(items)
        return render_template('index.html', view_model=item_view_model, current_user=current_user)

    @app.route('/add', methods=['POST'])
    @login_required
    def add():
        if current_user.role == ("writer"):
            add_item(title = request.form['myinput'])
            return redirect('/')
        else: 
            return redirect('/')
        
    @app.route('/update_todo', methods=['POST'])
    @login_required
    def update_todo():
        if current_user.role == ("writer"):
            update_items_todo(id = request.form['id'])
            return redirect('/')
        else: 
            return redirect('/')   

    @app.route('/update_doing', methods=['POST'])
    @login_required
    def update_doing():
        if current_user.role == ("writer"):
            update_items_doing(id = request.form['id'])
            return redirect('/')
        else: 
            return redirect('/')   

    @app.route('/update_done', methods=['POST'])
    @login_required
    def update_done():
        if current_user.role == ("writer"):
            update_items_done(id = request.form['id'])
            return redirect('/')
        else: 
            return redirect('/')   
    return app