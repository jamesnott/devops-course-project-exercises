import os, pytest, pymongo, mongomock, requests
from todo_app import app
from dotenv import load_dotenv, find_dotenv

@pytest.fixture
def client():
 file_path = find_dotenv('.env.test')
 load_dotenv(file_path, override=True)

 with mongomock.patch(servers=(('fakemongo.com', 27017),)):
    # here we add some fake data
    client = pymongo.MongoClient('mongodb://fakemongo.com')
    collection = client['jamnotdb']['posts']
    collection.insert_one({"_id": 1,
          "item":"Buy doors", 
          "status":"To Do"
    })
    collection.insert_one({"_id": 2,
          "item":"Fix tap", 
          "status":"Doing"
    })
    collection.insert_one({"_id": 3,
          "item":"Clean oven", 
          "status":"Done"
    })
    test_app = app.create_app()
    with test_app.test_client() as client:
        yield client

###################################################################
### Previous Tests  -  Left for reference            ##############
###################################################################

""" def client():
    # Use our test integration config instead of the 'real' version
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)

    # Create the new app.
    test_app = app.create_app()

    # Use the app to create a test_client that can be used in our tests.
    with test_app.test_client() as client:
        yield client """

""" class StubResponse():
    def __init__(self, fake_response_data):
        self.fake_response_data = fake_response_data
 
    def json(self):
        return self.fake_response_data """

# Stub replacement for requests.get(url)
""" def stub(url, params={}):
    test_board_id = os.environ.get('TRELLO_BOARD_ID')
    print (test_board_id)
    fake_response_data = None
    if url == f'https://api.trello.com/1/boards/{test_board_id}/lists':
        fake_response_data = [{
            'id': '123abc',
            'name': 'To Do',
            'cards': [{'id': '456', 'name': 'Test card'},{'id': '789', 'name': 'Go Shopping'}]
        }]
        return StubResponse(fake_response_data)

    raise Exception(f'Integration test did not expect URL "{url}"') """


def test_index_page(client):
    # Make a request to our app's index page
    response = client.get('/')

    assert response.status_code == 200
    assert "Buy doors" in response.data.decode()
    assert "Fix tap" in response.data.decode()
    assert "Clean oven" in response.data.decode()