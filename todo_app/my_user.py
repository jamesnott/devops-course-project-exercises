from flask_login import UserMixin

class User(UserMixin):
    def __init__(self, id):
        self.id = id       
        if id == "11882319":
            self.role = "writer"
        else:
            self.role = "reader"