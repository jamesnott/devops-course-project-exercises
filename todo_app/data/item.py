class Item:
    def __init__(self, id, name, status = 'To Do'):
        self.id = id
        self.name = name
        self.status = status

    @classmethod
    def from_mongo_document(cls, document):
        return cls(document['_id'], document['item'], document['status'])