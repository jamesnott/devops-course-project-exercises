import requests, pymongo, pprint, os
from todo_app.data.item import Item
from bson.objectid import ObjectId 

def get_collection():
    client = pymongo.MongoClient(os.getenv("MONGOURL")) 
    db = client['jamnotdb']
    collection = db['posts']
    return collection


def get_items():       
    lists_of_items: list[Item] = []

    for mongo_document in get_collection().find():
            item = Item.from_mongo_document(mongo_document)
            lists_of_items.append(item) 
    return lists_of_items

def add_item(title): 
    post = {
        "item": title,
        "status": "To Do"
    }
    post_id = get_collection().insert_one(post).inserted_id
    post_id

def update_items_todo(id):

     
    # Filter on ID of document
    filter = { '_id': ObjectId(id) }

    # assign values to be updated to a variable.
    newvalues = { "$set": { 'status': "To Do" } }

    # Use update_one() method for single update
    get_collection().update_one(filter, newvalues)
    
def update_items_doing(id):
    
    # Filter on ID of document
    filter = { '_id': ObjectId(id) }

    # change status of document to Doing
    newvalues = { "$set": { 'status': "Doing" } }

    # Using update_one() method for single update
    get_collection().update_one(filter, newvalues)
    
def update_items_done(id):
    
    # Filter on ID of document
    filter = { '_id': ObjectId(id) }

    # change status of document to Done
    newvalues = { "$set": { 'status': "Done" } }

    # Using update_one() method for single update
    get_collection().update_one(filter, newvalues)