from todo_app.data.view_model import ViewModel
from todo_app.data.item import Item

def test_view_model_todo_items_property_only_returns_todo_items():
    # Arrange
    items = [
        Item("1", "Task 1", "To Do"),
        Item("2", "Task 2", "Doing"),
        Item("3", "Task 3", "Done"),
    ]

    view_model = ViewModel(items)

    # Act
    returned_todo_items = view_model.todo_items

    # Assert
    assert len(returned_todo_items) == 1

    todo_item = returned_todo_items[0]

    assert todo_item.name == "Task 1"
    
    assert todo_item.status == "To Do"
    
def test_view_model_doing_items_property_only_returns_doing_items():
    # Arrange
    items = [
        Item("1", "Task 1", "To Do"),
        Item("2", "Task 2", "Doing"),
        Item("3", "Task 3", "Done"),
    ]

    view_model = ViewModel(items)

    # Act
    returned_doing_items = view_model.doing_items

    # Assert
    assert len(returned_doing_items) == 1

    doing_item = returned_doing_items[0]

    assert doing_item.name == "Task 2"
    
    assert doing_item.status == "Doing"

def test_view_model_done_items_property_only_returns_done_items():
    # Arrange
    items = [
        Item("1", "Task 1", "To Do"),
        Item("2", "Task 2", "Doing"),
        Item("3", "Task 3", "Done"),
    ]

    view_model = ViewModel(items)

    # Act
    returned_done_items = view_model.done_items

    # Assert
    assert len(returned_done_items) == 1

    done_item = returned_done_items[0]

    assert done_item.name == "Task 3"
    
    assert done_item.status == "Done"
